package com.peng.pengmall.product.vo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

/**
 * @author Purely
 * @date 2021/1/25 19:33
 */

//二级分类vo
@NoArgsConstructor
@AllArgsConstructor
@Data
public class Catalog2Vo {

    private String catalog1Id;//一级父分类id
    private List<Catalog3Vo> catalog3List;//三级子分类id
    private String id;//三级子分类里面的id
    private String name;//三级子分类里面的name

    /**
     * 三级分类vo
     */
    @NoArgsConstructor
    @AllArgsConstructor
    @Data
    public static class Catalog3Vo{
        private String catalog2Id;//二级父分类id
        private String id;
        private String name;
    }


}

