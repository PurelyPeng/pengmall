package com.peng.pengmall.search.constant;

/**
 * @author Purely
 * @date 2021/1/24 21:55
 */

public class EsConstant {

    public static final String PRODUCT_INDEX = "pengmall_product";
    public static final Integer PRODUCT_PAGESIZE = 2;
}

