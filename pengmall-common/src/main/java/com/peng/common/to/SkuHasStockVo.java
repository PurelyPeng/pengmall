package com.peng.common.to;

import lombok.Data;

/**
 * @author Purely
 * @date 2021/1/24 20:52
 */
@Data
public class SkuHasStockVo {
    private Long skuId;
    private  Boolean hasStock;//是否有库存
}

