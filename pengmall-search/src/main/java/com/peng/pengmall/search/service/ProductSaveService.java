package com.peng.pengmall.search.service;

import com.peng.common.to.es.SkuEsModel;

import java.io.IOException;
import java.util.List;

/**
 * @author Purely
 * @date 2021/1/24 21:47
 */
public interface ProductSaveService {

    boolean productStatusUp(List<SkuEsModel> skuEsModels) throws IOException;
}
