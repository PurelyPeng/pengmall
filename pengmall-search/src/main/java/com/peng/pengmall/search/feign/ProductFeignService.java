package com.peng.pengmall.search.feign;

import com.peng.common.utils.R;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * @author Purely
 * @date 2021/2/26 9:56
 */
@FeignClient("pengmall-product")
public interface ProductFeignService {

    @RequestMapping("product/attr/info/{attrId}")
    R attrInfo(@PathVariable("attrId") Long attrId);
}
