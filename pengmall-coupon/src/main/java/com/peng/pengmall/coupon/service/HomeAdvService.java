package com.peng.pengmall.coupon.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.peng.common.utils.PageUtils;
import com.peng.pengmall.coupon.entity.HomeAdvEntity;

import java.util.Map;

/**
 * 首页轮播广告
 *
 * @author PurelyPeng
 * @email 1941564112@qq.com
 * @date 2020-12-04 21:46:32
 */
public interface HomeAdvService extends IService<HomeAdvEntity> {

    PageUtils queryPage(Map<String, Object> params);
}

