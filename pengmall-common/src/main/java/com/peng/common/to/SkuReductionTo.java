package com.peng.common.to;

import lombok.Data;

import java.math.BigDecimal;
import java.util.List;

/**
 * sku的优惠、满减等信息
 * @author Purely
 * @date 2021/1/19 11:53
 */
@Data
public class SkuReductionTo {

    private Long skuId;
    private int fullCount;
    private BigDecimal discount;
    private int countStatus;
    private BigDecimal fullPrice;
    private BigDecimal reducePrice;
    private int priceStatus;
    private List<MemberPrice> memberPrice;
}

