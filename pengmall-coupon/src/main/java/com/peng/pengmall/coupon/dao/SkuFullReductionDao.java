package com.peng.pengmall.coupon.dao;

import com.peng.pengmall.coupon.entity.SkuFullReductionEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * 商品满减信息
 * 
 * @author PurelyPeng
 * @email 1941564112@qq.com
 * @date 2020-12-04 21:46:31
 */
@Mapper
public interface SkuFullReductionDao extends BaseMapper<SkuFullReductionEntity> {
	
}
