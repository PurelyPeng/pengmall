package com.peng.pengmall.member.dao;

import com.peng.pengmall.member.entity.MemberCollectSpuEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * 会员收藏的商品
 * 
 * @author PurelyPeng
 * @email 1941564112@qq.com
 * @date 2020-12-05 11:26:29
 */
@Mapper
public interface MemberCollectSpuDao extends BaseMapper<MemberCollectSpuEntity> {
	
}
