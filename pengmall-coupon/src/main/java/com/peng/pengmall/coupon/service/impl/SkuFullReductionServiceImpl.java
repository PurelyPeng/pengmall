package com.peng.pengmall.coupon.service.impl;

import com.peng.common.to.MemberPrice;
import com.peng.common.to.SkuReductionTo;
import com.peng.pengmall.coupon.entity.MemberPriceEntity;
import com.peng.pengmall.coupon.entity.SkuLadderEntity;
import com.peng.pengmall.coupon.service.MemberPriceService;
import com.peng.pengmall.coupon.service.SkuLadderService;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.peng.common.utils.PageUtils;
import com.peng.common.utils.Query;

import com.peng.pengmall.coupon.dao.SkuFullReductionDao;
import com.peng.pengmall.coupon.entity.SkuFullReductionEntity;
import com.peng.pengmall.coupon.service.SkuFullReductionService;


@Service("skuFullReductionService")
public class SkuFullReductionServiceImpl extends ServiceImpl<SkuFullReductionDao, SkuFullReductionEntity> implements SkuFullReductionService {

    @Autowired
    SkuLadderService skuLadderService;
    @Autowired
    SkuFullReductionService skuFullReductionService;
    @Autowired
    MemberPriceService memberPriceService;


    @Override
    public PageUtils queryPage(Map<String, Object> params) {
        IPage<SkuFullReductionEntity> page = this.page(
                new Query<SkuFullReductionEntity>().getPage(params),
                new QueryWrapper<SkuFullReductionEntity>()
        );

        return new PageUtils(page);
    }

    @Override
    public void saveSkuReduction(SkuReductionTo skuReductionTo) {
        //保存sku的优惠、满减等信息 会员模块下与sku、spu相关的表 sms-> sms_sku_ladder 、sms_sku_full_reduction
        //  sms_sku_ladder 打折
        SkuLadderEntity skuLadderEntity = new SkuLadderEntity();
        skuLadderEntity.setSkuId(skuReductionTo.getSkuId());
        skuLadderEntity.setFullCount(skuReductionTo.getFullCount()); //满几件打折
        skuLadderEntity.setDiscount(skuReductionTo.getDiscount());//打几折
        skuLadderEntity.setAddOther(skuReductionTo.getCountStatus());
//        skuLadderEntity.setPrice();//打折后的价格 ：需要根据sku的价格进行计算
        if (skuReductionTo.getFullCount() >0){
            //如果要打折再保存
            skuLadderService.save(skuLadderEntity);
        }

        //  sms_sku_full_reduction  满减
        SkuFullReductionEntity skuFullReductionEntity = new SkuFullReductionEntity();
        BeanUtils.copyProperties(skuReductionTo,skuFullReductionEntity);
        if (skuFullReductionEntity.getFullPrice().compareTo(new BigDecimal("0"))==1){//判断满减价格大于0 再保存
            this.save(skuFullReductionEntity);
        }

        //sms_member_perice    会员价格
        List<MemberPrice> memberPrice = skuReductionTo.getMemberPrice();
        List<MemberPriceEntity> collect = memberPrice.stream().map((item) -> {
            MemberPriceEntity memberPriceEntity = new MemberPriceEntity();
            memberPriceEntity.setSkuId(skuReductionTo.getSkuId());
            memberPriceEntity.setMemberLevelId(item.getId());
            memberPriceEntity.setMemberLevelName(item.getName());
            memberPriceEntity.setMemberPrice(item.getPrice());
            memberPriceEntity.setAddOther(1);
            return memberPriceEntity;
        }).filter(item->{
            return item.getMemberPrice().compareTo(new BigDecimal("0"))==1;
        }).collect(Collectors.toList());

        memberPriceService.saveBatch(collect);

    }

}
