package com.peng.pengmall.order.dao;

import com.peng.pengmall.order.entity.OrderEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * 订单
 * 
 * @author PurelyPeng
 * @email 1941564112@qq.com
 * @date 2020-12-05 11:36:14
 */
@Mapper
public interface OrderDao extends BaseMapper<OrderEntity> {
	
}
