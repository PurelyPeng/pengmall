package com.peng.pengmall.search.vo;

import com.peng.common.to.es.SkuEsModel;
import lombok.Data;

import java.util.List;

/**
 * 搜索返回的商品数据
 *
 * @author Purely
 * @date 2021/1/31 16:18
 */
@Data
public class SearchResult {

    //查询到的所有商品信息
    private List<SkuEsModel> products;
    //以下是分页信息
    private Integer pageNum;
    private Long total;
    private Integer totalPages;
    private List<Integer> pageNavs;


    private List<BrandVo> brands;//当前查询到的结果，所有涉及到的品牌
    private List<CatalogVo> catalogs;//当前查询到的结果，所有涉及到的分类
    private List<AttrVo> attrs;//当前查询到的结果，所有涉及到的属性

    //====================以上是返回给页面的所有信息================
    /* 面包屑导航数据 */
    private List<NavVo> navs;
    @Data
    public static class BrandVo{
        private Long brandId;
        private String brandName;
        private String brandImg;
    }
    @Data
    public static class AttrVo{
        private Long attrId;
        private String attrName;
        private List<String> attrValue;
    }
    @Data
    public static class CatalogVo{
        private Long catalogId;
        private String catalogName;
    }
    @Data
    public static class NavVo{
        private String navName;
        private String navValue;
        private String link;
    }
}

