package com.peng.pengmall.coupon.dao;

import com.peng.pengmall.coupon.entity.SeckillSkuRelationEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * 秒杀活动商品关联
 * 
 * @author PurelyPeng
 * @email 1941564112@qq.com
 * @date 2020-12-04 21:46:31
 */
@Mapper
public interface SeckillSkuRelationDao extends BaseMapper<SeckillSkuRelationEntity> {
	
}
