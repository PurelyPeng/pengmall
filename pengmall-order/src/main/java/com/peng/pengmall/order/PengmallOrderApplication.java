package com.peng.pengmall.order;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class PengmallOrderApplication {

    public static void main(String[] args) {
        SpringApplication.run(PengmallOrderApplication.class, args);
    }

}
