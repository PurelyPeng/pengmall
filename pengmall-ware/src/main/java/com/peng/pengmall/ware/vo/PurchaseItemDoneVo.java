package com.peng.pengmall.ware.vo;

import lombok.Data;

/**
 * @author Purely
 * @date 2021/1/20 14:04
 */
@Data
public class PurchaseItemDoneVo {
    /**
     * 封装
     * PurchaseDoneVo 的list数据  items: [{itemId:1,status:4,reason:""}]//完成/失败的需求详情
     */
    private Long itemId;
    private Integer status;
    private String reason;

}

