package com.peng.pengmall.coupon.dao;

import com.peng.pengmall.coupon.entity.CouponEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * 优惠券信息
 * 
 * @author PurelyPeng
 * @email 1941564112@qq.com
 * @date 2020-12-04 21:46:32
 */
@Mapper
public interface CouponDao extends BaseMapper<CouponEntity> {
	
}
