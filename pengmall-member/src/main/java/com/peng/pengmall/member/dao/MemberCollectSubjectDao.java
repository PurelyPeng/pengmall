package com.peng.pengmall.member.dao;

import com.peng.pengmall.member.entity.MemberCollectSubjectEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * 会员收藏的专题活动
 * 
 * @author PurelyPeng
 * @email 1941564112@qq.com
 * @date 2020-12-05 11:26:29
 */
@Mapper
public interface MemberCollectSubjectDao extends BaseMapper<MemberCollectSubjectEntity> {
	
}
