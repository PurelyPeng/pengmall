package com.peng.pengmall.product.vo;

import lombok.Data;

/**
 * @author Purely
 * @date 2021/1/18 13:09
 */
@Data
public class BrandVo {
    /**
     * 		"brandId": 0,
     * 		"brandName": "string",
     */
    private Long brandId;
    private String brandName;
}

