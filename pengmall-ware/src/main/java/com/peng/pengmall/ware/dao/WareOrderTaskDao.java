package com.peng.pengmall.ware.dao;

import com.peng.pengmall.ware.entity.WareOrderTaskEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * 库存工作单
 * 
 * @author PurelyPeng
 * @email 1941564112@qq.com
 * @date 2020-12-05 11:41:47
 */
@Mapper
public interface WareOrderTaskDao extends BaseMapper<WareOrderTaskEntity> {
	
}
