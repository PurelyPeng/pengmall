package com.peng.pengmall.ware.vo;

import lombok.Data;

/**
 * @author Purely
 * @date 2021/1/24 20:52
 */
@Data
public class SkuHasStockVo {
    private Long skuId;
    private  Boolean hasStock;//是否有库存
}

