package com.peng.pengmall.ware.dao;

import com.peng.pengmall.ware.entity.PurchaseDetailEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * 
 * 
 * @author PurelyPeng
 * @email 1941564112@qq.com
 * @date 2020-12-05 11:41:48
 */
@Mapper
public interface PurchaseDetailDao extends BaseMapper<PurchaseDetailEntity> {
	
}
