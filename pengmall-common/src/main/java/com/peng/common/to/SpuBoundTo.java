package com.peng.common.to;

import lombok.Data;

import java.math.BigDecimal;

/**
 * @author Purely
 * @date 2021/1/19 11:35
 */
@Data
public class SpuBoundTo {

    private Long spuId;
    private BigDecimal buyBounds;
    private BigDecimal growBounds;
}

