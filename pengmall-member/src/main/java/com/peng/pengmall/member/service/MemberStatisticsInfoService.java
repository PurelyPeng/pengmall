package com.peng.pengmall.member.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.peng.common.utils.PageUtils;
import com.peng.pengmall.member.entity.MemberStatisticsInfoEntity;

import java.util.Map;

/**
 * 会员统计信息
 *
 * @author PurelyPeng
 * @email 1941564112@qq.com
 * @date 2020-12-05 11:26:29
 */
public interface MemberStatisticsInfoService extends IService<MemberStatisticsInfoEntity> {

    PageUtils queryPage(Map<String, Object> params);
}

