package com.peng.pengmall.product.vo;

import lombok.Data;

/**
 * @author Purely
 * @date 2021/1/17 20:36
 */
@Data
public class AttrGroupRelationVo {
    //[{"attrId":1,"attrGroupId":2}]
    private Long attrId;
    private Long attrGroupId;
}

