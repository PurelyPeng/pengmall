package com.peng.pengmall.search.service;

import com.peng.pengmall.search.vo.SearchParam;
import com.peng.pengmall.search.vo.SearchResult;

/**
 * @author Purely
 * @date 2021/1/31 15:56
 */
public interface MallSearchService {

    /**
     * 检索的所有参数
     * @param param
     * @return 检索 的结果
     */
    SearchResult search(SearchParam param);
}
