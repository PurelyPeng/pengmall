package com.peng.pengmall.product.dao;

import com.peng.pengmall.product.entity.SpuImagesEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * spu图片
 * 
 * @author PurelyPeng
 * @email 1941564112@qq.com
 * @date 2020-12-04 21:13:11
 */
@Mapper
public interface SpuImagesDao extends BaseMapper<SpuImagesEntity> {
	
}
