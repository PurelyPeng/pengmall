package com.peng.pengmall.ware.feign;

import com.peng.common.utils.R;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * @author Purely
 * @date 2021/1/20 15:00
 */
@FeignClient("pengmall-product")
public interface ProductFeignService {
    /**
     *
     * /product/skuinfo/info/{skuId}
     * /api//product/skuinfo/info/{skuId}
     *  1.让所有请求过网关：
     *      1.1 @FeignClient("pengmall-product") 给 pengmall-gateway所在的机器发请求
     *      1.2/api//product/skuinfo/info/{skuId}
     *  2.直接让后台指定服务处理
     *      2.1 @FeignClient("pengmall-product")
     *      /product/skuinfo/info/{skuId}
     */
    @RequestMapping("/product/skuinfo/info/{skuId}")
    public R info(@PathVariable("skuId") Long skuId);

}
