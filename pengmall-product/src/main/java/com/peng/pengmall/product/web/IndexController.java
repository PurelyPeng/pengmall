package com.peng.pengmall.product.web;

import com.peng.pengmall.product.entity.CategoryEntity;
import com.peng.pengmall.product.service.CategoryService;
import com.peng.pengmall.product.vo.Catalog2Vo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;
import java.util.Map;

/**
 * @author Purely
 * @date 2021/1/25 13:58
 */

@Controller
public class IndexController {

    @Autowired
    CategoryService categoryService;

    @GetMapping({"/", "index.html"})
    public String indexPage(Model model){
        //获取所有的一级分类
        List<CategoryEntity> categoryEntities = categoryService.getLevel1categorys();
        model.addAttribute("categorys",categoryEntities);
        return "index";
    }

    // /index/catalog.json
    @ResponseBody //将返回的值以json的方式返回出去
    @GetMapping("/index/catalog.json")
    public Map<String,List<Catalog2Vo>> getCatalogJson(){
        Map<String,List<Catalog2Vo>> catalogJson = categoryService.getCatalogJson();
        return catalogJson;
    }
}

