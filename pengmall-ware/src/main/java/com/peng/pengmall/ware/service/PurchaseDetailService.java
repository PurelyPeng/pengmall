package com.peng.pengmall.ware.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.peng.common.utils.PageUtils;
import com.peng.pengmall.ware.entity.PurchaseDetailEntity;

import java.util.List;
import java.util.Map;

/**
 *
 *
 * @author PurelyPeng
 * @email 1941564112@qq.com
 * @date 2020-12-05 11:41:48
 */
public interface PurchaseDetailService extends IService<PurchaseDetailEntity> {

    PageUtils queryPage(Map<String, Object> params);

    List<PurchaseDetailEntity> listDetailByPurchaseId(Long id);
}

