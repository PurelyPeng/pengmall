package com.peng.pengmall.product.feign;

import com.peng.common.to.SkuHasStockVo;
import com.peng.common.utils.R;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

import java.util.List;

/**
 * @author Purely
 * @date 2021/1/24 21:07
 */
@FeignClient("pengmall-ware")
public interface WareFeignService {

    /**
     * 查询sku是否有库存
     * /ware/waresku/list
     *
     */
    /**
     * 1.R设计的时候可以加上泛型  详情看本类和WareSkuController、R 或者  视频P132 十分钟左右处
     * 2.直接返回我们想要的结果  详情看视频P132 十分钟左右处
     * 3.自己封装解析结果       详情看视频P132 十分钟左右处
     * @param skuIds
     * @return
     */
    //库存服务的完整路径
    @PostMapping("/ware/waresku/hasstock")
     R getSkusHasStock(@RequestBody List<Long> skuIds);

}
