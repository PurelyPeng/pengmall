package com.peng.pengmall.coupon.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.peng.common.utils.PageUtils;
import com.peng.pengmall.coupon.entity.CouponEntity;

import java.util.Map;

/**
 * 优惠券信息
 *
 * @author PurelyPeng
 * @email 1941564112@qq.com
 * @date 2020-12-04 21:46:32
 */
public interface CouponService extends IService<CouponEntity> {

    PageUtils queryPage(Map<String, Object> params);
}

