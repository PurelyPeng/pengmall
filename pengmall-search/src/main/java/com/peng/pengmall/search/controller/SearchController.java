package com.peng.pengmall.search.controller;

import com.peng.pengmall.search.service.MallSearchService;
import com.peng.pengmall.search.vo.SearchParam;
import com.peng.pengmall.search.vo.SearchResult;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

import javax.servlet.http.HttpServletRequest;

/**
 * @author Purely
 * @date 2021/1/30 19:54
 */

@Controller
public class SearchController {

    @Autowired
    MallSearchService mallSearchService;

    @GetMapping("/list.html")
    public String listPage(SearchParam param, Model model, HttpServletRequest request){

        param.set_queryString(request.getQueryString());
        //1.根据传递来的页面的查询参数，去es中检索商品并且返回页面需要的数据
        SearchResult result = mallSearchService.search(param);
        model.addAttribute("result",result);
        return "list";
    }
}

